FROM debian:buster
RUN apt-get update && apt-get install -y build-essential git \ 
	libncurses-dev zlib1g-dev wget python2.7 unzip gawk file \
	curl uuid-dev libpopt-dev xsltproc intltool gdisk python3 \
	rsync libglib2.0-dev u-boot-tools libssl-dev qemu-utils
RUN useradd -m build
USER build
